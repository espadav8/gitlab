# frozen_string_literal: true

module Search
  module Zoekt
    class IndexingTaskWorker
      include ApplicationWorker
      include Search::Worker
      prepend ::Geo::SkipSecondary

      data_consistency :delayed
      idempotent!
      pause_control :deprecated
      urgency :low

      def perform(project_id, task_type, options = {})
        return false unless ::Gitlab::CurrentSettings.zoekt_indexing_enabled?
        return false unless ::License.feature_available?(:zoekt_code_search)

        options = options.with_indifferent_access
        keyword_args = {
          node_id: options[:node_id], force: options[:force], delay: options[:delay],
          root_namespace_id: options[:root_namespace_id]
        }.compact
        IndexingTaskService.execute(project_id, task_type, **keyword_args)
      end
    end
  end
end
